/* Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 */

/* 
 * ptable.hpp
 * 
 * This file is part of the cdiffusion distribution (https://gitlab.com/ladeynov.d/stormer-verlet).
 * Copyright (c) 2021 Dmitry Ladeynov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <unordered_map>

std::unordered_map<std::string, std::string> ptable = {

// Input/Output files parameters

    { "ca_matrix_out",       "matrix_"    },
    { "sources_matrix_in",   "sources"    },
    { "cell_constr_in",      "mask"       },
    { "itf_matrix_in",       "itf_matrix" },
    { "ca_matrix_in",        "ca_matrix"  },

    { "file_format",         ".txt"       },

    { "output_path",         "result/"    },

    { "input_path",          "init/"      },

// Modeling parameters ( sec. )

    { "time_step",           "0.01"       },
    { "simulation_time",     "1.0"        },

// Diffusion calcium coeficient ( mkm^2 / sec )

    { "coef_diff_itf",       "10.0"       },
    { "coef_diff_ca",        "30.0"       },

// Image parameters 
    { "rl_side_scale",       "113.8"      },
    { "im_side_scale",       "512.0"      },

// Astrocyte system parameters

    { "z",                   "0.8798"     },
    { "q",                   "0.0849"     },
    { "p",                   "1.0514"     },
    { "p_1",                 "0.5"        },
    { "v4",                  "0.3"        },
    { "v3",                  "2.2"        },
    { "v2",                  "0.11"       },
    { "v1",                  "6.0"        },
    { "tau_ip",              "7.143"      },
    { "d5",                  "0.082"      },
    { "d2",                  "1.049"      },
    { "d1",                  "0.13"       },
    { "D",                   "0.2"        },
    { "c1",                  "0.185"      },
    { "c0",                  "2.0"        },
    { "alf",                 "0.8"        },
    { "a2",                  "0.14"       }

};
