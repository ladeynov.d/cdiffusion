/* Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 */

/* 
 * main.cpp
 * 
 * This file is part of the cdiffusion distribution (https://gitlab.com/ladeynov.d/stormer-verlet).
 * Copyright (c) 2021 Dmitry Ladeynov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <chrono>
#include <random>
#include <iostream>
#include <iomanip>
#include "omp.h"
#include "io_func.hpp"
#include "ptable.hpp"
#include "matrix_operation.hpp"
#include "astrocytes.hpp"
#include "cadiffusion.hpp"

void print_parameters(std::unordered_map<std::string, std::string> pt, uint32_t frame, uint32_t itr) {

    std::cout << std::left << "Astrocyte system parameters\n\n";
    for(const auto& i:pt)
        std::cout << std::left << std::setw(26) << i.first  << i.second     << "\n";
}

int main(int argc, char* argv[])
{
    const uint32_t blk_size(10);

    std::string file_param = "parameters.cfg";

    uint32_t frame       = 1;
    uint32_t iteration   = 1;

    uint32_t num_threads = omp_get_max_threads();

    if (argc)
        file_param  = argv[1];

    std::cout<< "Load parameters: ";
    read_parameters(ptable,  file_param);
    std::cout<< "Success\n\n";
    iteration = diff_iteration(std::stod(ptable["im_side_scale"]),
                               std::stod(ptable["rl_side_scale"]),
                               std::stod(ptable["coef_diff_ca"]),
                               std::stod(ptable["time_step"]),
                               blk_size);

    frame = static_cast<uint32_t>(std::stod(ptable["simulation_time"]) / 
                                  std::stod(ptable["time_step"]));

    print_parameters(ptable, frame, iteration);

    std::vector<std::vector<uint16_t>> i_matrix;
    std::vector<std::vector<uint16_t>> i_mask;
    std::vector<std::vector<uint16_t>> i_sources;

    std::vector<std::vector<uint8_t>>  matrix;
    std::vector<std::vector<uint8_t>>  mask;
    std::vector<std::vector<uint8_t>>  temp_blk;
    std::vector<std::vector<uint32_t>> coordinates;

    std::random_device r;
    std::vector<std::mt19937_64> generators;
    for( uint32_t i = 0; i < num_threads; i++ ) 
        generators.emplace_back(std::mt19937_64(r()));

    std::cout<< "Load matrix: ";
    open_matrix(i_matrix,  ptable["input_path"] + ptable["ca_matrix_in"] + ptable["file_format"]);
    open_matrix(i_mask,    ptable["input_path"] + ptable["cell_constr_in"] + ptable["file_format"]);
    open_matrix(i_sources, ptable["input_path"] + ptable["sources_matrix_in"] + ptable["file_format"]);
    std::cout<< "Success\n\n";

    auto output_matrix_path = ptable["output_path"] + ptable["ca_matrix_out"];
    auto file_format = ptable["file_format"];

    const uint32_t msize( blk_size * i_matrix.size() );

    create_matrix(matrix, i_matrix, blk_size);
    create_mask(mask, i_mask, blk_size, msize);
    create_coordinates(coordinates, i_sources);

    uint32_t coord_size(coordinates.size());

    std::vector<AstroObj<double, uint32_t>> astrocytes(coord_size);

    for(uint32_t i = 0; i < coord_size; i++) {
        astrocytes[i].set_i(coordinates[i][0]);
        astrocytes[i].set_j(coordinates[i][1]);
        astrocytes[i].set_n(i);
        astrocytes[i].set_p(std::stod(ptable["p"])); 
        astrocytes[i].set_q(std::stod(ptable["q"]));
        astrocytes[i].set_z(std::stod(ptable["z"]));
        astrocytes[i].set_D(std::stod(ptable["D"]));
        astrocytes[i].set_a2(std::stod(ptable["a2"]));
        astrocytes[i].set_c0(std::stod(ptable["c0"]));
        astrocytes[i].set_c1(std::stod(ptable["c1"]));
        astrocytes[i].set_d1(std::stod(ptable["d1"]));
        astrocytes[i].set_d2(std::stod(ptable["d2"]));
        astrocytes[i].set_d5(std::stod(ptable["d5"]));
        astrocytes[i].set_v1(std::stod(ptable["v1"]));
        astrocytes[i].set_v2(std::stod(ptable["v2"]));
        astrocytes[i].set_v3(std::stod(ptable["v3"]));
        astrocytes[i].set_v4(std::stod(ptable["v4"]));
        astrocytes[i].set_alf(std::stod(ptable["alf"]));
        astrocytes[i].set_p_1(std::stod(ptable["p_1"]));
        astrocytes[i].set_tS(std::stod(ptable["time_step"]));
    }

// main algorithm >>>

    for( uint32_t i = 0; i < frame; i++ ) {

        auto begin = std::chrono::high_resolution_clock::now();

        result_matrix(i_matrix, matrix, blk_size);
        fwrite_matrix(i_matrix, i, output_matrix_path, file_format);

        for(auto& ias: astrocytes) {

            auto im = ias.get_i();
            auto jm = ias.get_j();

            ias.set_Q(static_cast<double>(i_matrix[im][jm]) / 100.0 );

            ias.step_runge_kutt();

            temp_blk.clear();
            shuffle_block(temp_blk, static_cast<uint8_t>(ias.get_q()*100.0), blk_size);
            temp_blk[4][5] = 1;

            for(uint32_t it = 0; it < blk_size; it++)
                for(uint32_t jt = 0; jt < blk_size; jt++)
                    matrix[it + im * blk_size]
                          [jt + jm * blk_size] = temp_blk[it][jt];
        }

        for(uint32_t j = 0; j < iteration; j++) {
            #pragma omp parallel num_threads(num_threads)
            diffusion(matrix, mask, generators[omp_get_thread_num()], msize);
        }

        auto end = std::chrono::high_resolution_clock::now();
        auto cur_time = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
        std::cout << std::left << std::setw(8) << "frame: " << i << " | "
                  << "time: " << cur_time << " ms" << std::endl;
    }

// main algorithm <<<

    return 0;
}




