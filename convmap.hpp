/* Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 */

/* 
 * convmap.hpp
 * 
 * This file is part of the cdiffusion distribution (https://gitlab.com/ladeynov.d/stormer-verlet).
 * Copyright (c) 2021 Dmitry Ladeynov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstdint>

/*
    Pattern Map
    ---------------------------------------------
    |   state  |  value   |   state  |   value  |
    ---------------------------------------------
    |     0    |   0  0   |     8    |   1  0   |
    | ( 0000 ) |   0  0   | ( 1000 ) |   0  0   |
    ----------------------------------------------
    |     1    |   0  0   |     9    |   1  0   |
    | ( 0001 ) |   0  1   | ( 1001 ) |   0  1   |
    ---------------------------------------------
    |     2    |   0  0   |    10    |   1  0   |
    | ( 0010 ) |   1  0   | ( 1010 ) |   1  0   |
    ---------------------------------------------
    |     3    |   0  0   |    11    |   1  0   |
    | ( 0011 ) |   1  1   | ( 1011 ) |   1  1   |
    ---------------------------------------------
    |     4    |   0  1   |    12    |   1  1   |
    | ( 0100 ) |   0  0   | ( 1100 ) |   0  0   |
    ---------------------------------------------
    |     5    |   0  1   |    13    |   1  1   |
    | ( 0101 ) |   0  1   | ( 1101 ) |   0  1   |
    ---------------------------------------------
    |     6    |   0  1   |    14    |   1  1   |
    | ( 0110 ) |   1  0   | ( 1110 ) |   1  0   |
    ---------------------------------------------
    |     7    |   0  1   |    15    |   1  1   |
    | ( 0111 ) |   1  1   | ( 1111 ) |   1  1   |
    ---------------------------------------------

*/

static const uint8_t stmap[2][16][4] = 
{
    {
        {0, 0, 0, 0},    // 0    0
        {0, 1, 0, 0},    // 4    1
        {0, 0, 0, 1},    // 1    2
        {0, 1, 0, 1},    // 5    3
        {1, 0, 0, 0},    // 8    4
        {1, 1, 0, 0},    // 12   5
        {1, 0, 0, 1},    // 9    6
        {1, 1, 0, 1},    // 13   7
        {0, 0, 1, 0},    // 2    8
        {0, 1, 1, 0},    // 6    9
        {0, 0, 1, 1},    // 3    10
        {0, 1, 1, 1},    // 7    11
        {1, 0, 1, 0},    // 10   12
        {1, 1, 1, 0},    // 14   13
        {1, 0, 1, 1},    // 11   14
        {1, 1, 1, 1}     // 15   15
    },{
        {0, 0, 0, 0},    // 0    0
        {0, 0, 1, 0},    // 2    1
        {1, 0, 0, 0},    // 8    2
        {1, 0, 1, 0},    // 10   3
        {0, 0, 0, 1},    // 1    4
        {0, 0, 1, 1},    // 3    5
        {1, 0, 0, 1},    // 9    6
        {1, 0, 1, 1},    // 11   7
        {0, 1, 0, 0},    // 4    8
        {0, 1, 1, 0},    // 6    9
        {1, 1, 0, 0},    // 12   10
        {1, 1, 1, 0},    // 14   11
        {0, 1, 0, 1},    // 5    12
        {0, 1, 1, 1},    // 7    13
        {1, 1, 0, 1},    // 13   14 
        {1, 1, 1, 1}     // 15   15
    }
};

