/* Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 */

/* 
 * matrix_operation.hpp
 * 
 * This file is part of the cdiffusion distribution (https://gitlab.com/ladeynov.d/stormer-verlet).
 * Copyright (c) 2021 Dmitry Ladeynov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <fstream>
#include <sstream>
#include <algorithm>
#include <string>
#include <random>
#include <vector>
#include "astrocytes.hpp"

template < class T1, class T2 >
void shuffle_block(std::vector<std::vector<T1>>& mtx, T2 percent, const uint32_t blk_size) {

    auto jt = mtx.begin();

    for (auto j = 0U; j < blk_size; j++) {

        mtx.emplace_back();
        jt = mtx.end() - 1;

        for (auto i = 0U; i < blk_size; i++) {
            if (percent != 0) {
                (*jt).push_back(1);
                percent--;
            } else {
                (*jt).push_back(0);
            }
        }
    }

    std::shuffle(mtx.begin(), mtx.end(), std::mt19937(std::random_device()()) );
}

template < class T >
void glue_vector_v(std::vector<std::vector<T>>& mtx1,
                   std::vector<std::vector<T>>& mtx2) {

    if(mtx1.empty())
        mtx1 = mtx2;
    else
        mtx1.insert(mtx1.end(), mtx2.begin(), mtx2.end());
}

template < class T >
void glue_vector_h(std::vector<std::vector<T>>& mtx1,
                   std::vector<std::vector<T>>& mtx2) {

    if(mtx1.empty())
        mtx1 = mtx2;
    else
        for(auto jt1 = mtx1.begin(), jt2 = mtx2.begin(); jt1 != mtx1.end(); ++jt1, ++jt2)
            (*jt1).insert((*jt1).end(), (*jt2).begin(), (*jt2).end());
}

template < class T1, class T2 >
void cblock_msk(std::vector<std::vector<T1>>& out, T2 x, const uint32_t blk_size) {

    std::vector<T1> one (blk_size);
    std::fill(one.begin(), one.end(), static_cast<T1>(!x));
    std::fill(out.begin(), out.end(), one);

}

template <class T1, class T2 >
void create_mask(std::vector<std::vector<T1>>& out,
                 std::vector<std::vector<T2>>& imask, 
                 const uint32_t blk_size, const uint32_t size) {

    std::vector<std::vector<T1>> hblock;
    std::vector<std::vector<T1>> temp(blk_size);

    for(auto & i: imask) {
        for(auto & j: i) {

            cblock_msk(temp, j, blk_size);
            glue_vector_h(hblock, temp);
        }
        glue_vector_v(out, hblock);
        hblock.clear();
    }

    for( uint32_t i = 0; i < size - 2; i += 2 )
       for( uint32_t j = 0; j < size - 2; j += 2 )
           out[ i + 1 ][ j + 1 ] =  out[ i + 1 ][ j + 1 ] ||
                                    out[ i + 1 ][ j + 2 ] ||
                                    out[ i + 2 ][ j + 1 ] ||
                                    out[ i + 2 ][ j + 2 ];

}

template < class T1, class T2 >
void create_matrix(std::vector<std::vector<T1>>& out,
                   std::vector<std::vector<T2>>& imtx, const uint32_t blk_size) {

    std::vector<std::vector<T1>> hblock;
    std::vector<std::vector<T1>> temp;

    for(auto & i: imtx) {
        for(auto & j: i) {

            shuffle_block(temp, j, blk_size);
            glue_vector_h(hblock, temp);
            temp.clear();
        }
        glue_vector_v(out, hblock);
        hblock.clear();
    }
}

template < class T1, class T2 >
inline void block_sum(std::vector<std::vector<T1>>& mtx, uint32_t xidx,
                      uint32_t yidx, const uint32_t blk_size, T2& sum) {
    sum = 0;
    for(auto i = mtx.begin() + yidx; i < mtx.begin() + yidx + blk_size; ++i)
        sum += std::accumulate((*i).begin() + xidx, (*i).begin() + xidx + blk_size, 0U);

}

template < class T1, class T2 >
void result_matrix(std::vector<std::vector<T1>>& out,
                   std::vector<std::vector<T2>>& mtx, const uint32_t blk_size) {

    T1 x_shift_idx = 0;
    T1 y_shift_idx = 0;

    for(auto & it: out) {
        for(auto & jt: it) {

            block_sum(mtx, x_shift_idx, y_shift_idx, blk_size, jt);
            x_shift_idx += blk_size;
        }
        x_shift_idx = 0;
        y_shift_idx += blk_size;
    }
}

template < class T1, class T2 >
void create_coordinates(std::vector<std::vector<T1>>& out, std::vector<std::vector<T2>>& in) {

    uint32_t msize = in.size();

    for(uint32_t i = 0; i < msize; i++)
        for(uint32_t j = 0; j < msize; j++)
            if(in[i][j] == 1)
                out.push_back({i, j});
}
