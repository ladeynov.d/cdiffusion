/* Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 */

/* 
 * io_func.hpp
 * 
 * This file is part of the cdiffusion distribution (https://gitlab.com/ladeynov.d/stormer-verlet).
 * Copyright (c) 2021 Dmitry Ladeynov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <iostream>
#include <iterator>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <regex>

template <class T>
std::ostream& operator<<(std::ostream& s, const std::vector<std::vector<T>>& m) {

    for(const auto& i: m) {
        for(const auto& j :i)
            s << static_cast<int>(j) << ' ';
        s << '\n';
    }
    return s;
}

template <class T>
void open_matrix(std::vector<std::vector<T>>& matrix, const std::string path ) {

    std::string str;
    std::regex digit("([\\d]+]?)");
    std::smatch result;
    std::ifstream file(path);

    if (!file) {
        std::cout << '\n'<< path << "    File dose't exist\n";
        return;
    }

    auto it = matrix.end();

    while(!file.eof())
    {
        matrix.push_back({});
        it = matrix.end() - 1;

        std::getline(file, str);

        while (std::regex_search (str, result, digit)) {

            (*it).push_back(std::stoi(result[0]));
            str = result.suffix().str();
        }
    }

    matrix.pop_back();
    file.close();
}

template <class T>
void fwrite_matrix(const std::vector<std::vector<T>>& matrix, uint32_t num,
                   const std::string path, const std::string format) {

//    std::cout << num << ' ';
    std::string file_result = path;
    file_result.append(std::to_string(num));
    file_result.append(format);

    std::ofstream fout(file_result);
    std::string res;

    for(auto & i: matrix) {
        for(auto & j: i) {

            res.append(std::to_string(static_cast<uint32_t>(j)));
            res.push_back(' ');
        }
        fout << res << '\n';
        res.clear();
    }

    fout.close();
}

void read_parameters(std::unordered_map<std::string, std::string>& prms, const std::string path ) {

    std::string   str;
    std::regex    equality("(\\s+=\\s+)");
    std::smatch   result;
    std::ifstream file(path);

    if (!file) {
        std::cout << path << "\nFile dose't exist\n";
        return;
    }

    while(!file.eof()) {

        std::getline(file, str);

        if(std::regex_search (str, result, equality)) {
            auto it = prms.find(result.prefix());
            if(it != prms.end()) 
                it->second = result.suffix();
        }
    }
    file.close();
}
