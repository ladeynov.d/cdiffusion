/* Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 */

/* 
 * astrocytes.hpp
 * 
 * This file is part of the cdiffusion distribution (https://gitlab.com/ladeynov.d/stormer-verlet).
 * Copyright (c) 2021 Dmitry Ladeynov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>
#include <cmath>

template <class FP, class UI>
class AstroObj {

    FP alf = 0.8;
    FP a2  = 0.14;

    FP c0  = 2.0, c1  = 0.185;

    FP k_2 = 1.0,  k_3 = 0.1,   k_4 = 1.1;
    FP d1  = 0.13, d2  = 1.049, d3  = 0.9434, d5 = 0.082;
    FP v1  = 6.0,  v2  = 0.11,  v3 = 2.2, v4  = 0.3;
    FP p_1 = 0.5;
    FP timeStep = 0.01;
    FP tau_ip = 7.143;
    FP p  = 1.0514, q  = 0.0849, z  = 0.8798, Q = 0.0;
    FP D = 0.2;

    std::array<FP, 12> k = { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0. };

    UI i = 0, j = 0, n = 0;

private:

    FP Jchannel( FP _p, FP _q, FP _z) {
        return c1*v1*(pow((_p*_q*_z), 3.))*((c0 / c1) - (1. + (1. / c1))*_q)\
            / (pow((d1 + _p)*(d5 + _q), 3.)); };

    FP Jpump( FP _q) { return v3*_q*_q / (k_3*k_3 + _q*_q); };

    FP Jleak( FP _q) { return c1*v2*((c0 / c1) - (1. + (1. / c1))*_q); };

    FP  Jplc( FP _q) { return v4*(_q + (1. - alf)*k_4) / (_q + k_4); };


public:

    void  set_p( FP val )      { p   = val; }
    void  set_q( FP val )      { q   = val; }
    void  set_z( FP val )      { z   = val; }
    void  set_D( FP val )      { D   = val; }
    void  set_Q( FP val )      { Q   = val; }
    void  set_a2( FP val )     { a2  = val; }
    void  set_c0( FP val )     { c0  = val; }
    void  set_c1( FP val )     { c1  = val; }
    void  set_d1( FP val )     { d1  = val; }
    void  set_d2( FP val )     { d2  = val; }
    void  set_d5( FP val )     { d5  = val; }
    void  set_v1( FP val )     { v1  = val; }
    void  set_v2( FP val )     { v2  = val; }
    void  set_v3( FP val )     { v3  = val; }
    void  set_v4( FP val )     { v4  = val; }
    void  set_alf( FP val )    { alf = val; }
    void  set_p_1( FP val )    { p_1 = val; }
    void  set_tS( FP val )     { timeStep = val; }
    void  set_tau_ip( FP val ) { tau_ip   = val; }
    void  set_n( UI _n )  { n  = _n;  }
    void  set_i( UI _i )  { i  = _i;  }
    void  set_j( UI _j )  { j  = _j;  }

    FP get_p() { return p; };
    FP get_q() { return q; };
    FP get_z() { return z; };
    UI get_i() { return i; };
    UI get_j() { return j; };
    UI get_n() { return n; };

    void step_runge_kutt() {

        k[0]  = ( (p_1 - p) / tau_ip + Jplc(q) )*timeStep;
        k[4]  = ( Jchannel(p, q, z) - Jpump(q) + Jleak(q) + D*(Q - q))*timeStep;
        k[8]  = ( a2*(d2*((p + d1) / (p + d3))*(1. - z) - z*q) )*timeStep;

        k[1]  = ( (p_1 - (p + 0.5*k[0])) / tau_ip + Jplc(q + 0.5*k[4]) )*timeStep;
        k[5]  = ( Jchannel(p + 0.5*k[0], q + 0.5*k[4], z + 0.5*k[8]) - Jpump(q + 0.5*k[4]) + Jleak(q + 0.5*k[4]) + D*(Q - q + 0.5*k[4] ))*timeStep;
        k[9]  = ( a2*(d2*(((p + 0.5*k[0]) + d1) / ((p + 0.5*k[0]) + d3))*(1. - (z + 0.5*k[8])) - (z + 0.5*k[8])*(q + 0.5*k[4])) )*timeStep;

        k[2]  = ( (p_1 - (p + 0.5*k[1])) / tau_ip + Jplc(q + 0.5*k[5]) )*timeStep;
        k[6]  = ( Jchannel(p + 0.5*k[1], q + 0.5*k[5], z + 0.5*k[9]) - Jpump(q + 0.5*k[5]) + Jleak(q + 0.5*k[5]) + D*(Q - q + 0.5*k[5]) )*timeStep;
        k[10] = ( a2*(d2*(((p + 0.5*k[1]) + d1) / ((p + 0.5*k[1]) + d3))*(1. - (z + 0.5*k[9])) - (z + 0.5*k[9])*(q + 0.5*k[5])) )*timeStep;

        k[3]  = ( (p_1 - (p + k[2])) / tau_ip + Jplc(q + k[6]) )*timeStep;
        k[7]  = ( Jchannel(p + k[2], q + k[6], z + k[10]) - Jpump(q + k[6]) + Jleak(q + k[6]) + D*(Q - q + k[6]))*timeStep;
        k[11] = ( a2*(d2*(((p + k[2]) + d1) / ((p + k[2]) + d3))*(1. - (z + k[10])) - (z + k[10])*(q + k[6])) )*timeStep;

        p = p + (1.0 / 6.0)*(k[0] + 2.0 * k[1] + 2.0 * k[2]  + k[3]);
        q = q + (1.0 / 6.0)*(k[4] + 2.0 * k[5] + 2.0 * k[6]  + k[7]);
        z = z + (1.0 / 6.0)*(k[8] + 2.0 * k[9] + 2.0 * k[10] + k[11]);

        }
};
