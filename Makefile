APP = diffusion

CXX=g++

CXXFLAGS=-c -Wall -std=c++2a -O3
LDFLAGS=

CXXFLAGS+=-fopenmp
LDFLAGS+=-fopenmp

#CXXFLAGS+=-I/usr/include/
#GSL_LINK=-L/usr/lib/x86_64-linux-gnu/ -lgsl -lgslcblas -lm

CXXFLAGS+=-D_GLIBCXX_PARALLEL 
CXXFLAGS+=-march=native 
CXXFLAGS+=-fopt-info-vec-optimized  
CXXFLAGS+=-funroll-all-loops 
CXXFLAGS+=-unroll-loops 
CXXFLAGS+=-omit-frame-pointer 
CXXFLAGS+=-mavx

SRCS=$(wildcard *.cpp)
APP_OBJS = $(SRCS:.cpp=.o)

all: $(SRCS) $(APP)

$(APP): $(APP_OBJS)
	$(CXX) $(LDFLAGS) $(APP_OBJS) -o $@

$(SRCS).o:
	$(CXX) $(CXXFLAGS) $< -o $@

install: $(APP)
	mv $(APP) ../

clean:
	rm -rf *.o $(APP) *.elf *.gdb
