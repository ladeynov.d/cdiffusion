/* Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 */

/* 
 * cadiffusion.hpp
 * 
 * This file is part of the cdiffusion distribution (https://gitlab.com/ladeynov.d/stormer-verlet).
 * Copyright (c) 2021 Dmitry Ladeynov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <random>
#include <vector>
#include <cmath>
#include "convmap.hpp"
#include "omp.h"

template< class T>
uint32_t diff_iteration(T im_sc_side, T rl_sc_side, T c_diff, T timeStep, uint32_t blk_size ) {

    return static_cast<uint32_t>(std::round((c_diff * pow( im_sc_side, 2) *
        std::pow(blk_size, 2) * timeStep ) / std::pow(rl_sc_side, 2)));
}

template< class T>
inline void rotate(T& b3, T& b2, T& b1, T& b0, const T rnd, const T bitmask) {

    if(!bitmask) {
        auto temp = stmap[ rnd ][ b3 * 8 + b2 * 4 + b1 * 2 + b0 ];
        b0 = temp[0];
        b1 = temp[1];
        b2 = temp[2];
        b3 = temp[3];
    }
}

template< class T>
void diffusion(std::vector<std::vector<T>>& mtx, std::vector<std::vector<T>>& msk,
               std::mt19937_64 & generator, const uint32_t size) 
{
    uint32_t kmax = 64;

    #pragma omp for simd schedule(static, 10)
    for( uint32_t i = 0; i < size - 1; i += 2 )
        for( uint32_t j = 0; j < size - 1; j += 2 * kmax ) {
            uint64_t r = generator();
            for(uint32_t k = 0; k < kmax; k++ ) {
                rotate( mtx[ i ][ j + 2 * k ],     mtx[ i ][ j + 1 + 2 * k ],
                        mtx[ i + 1 ][ j + 2 * k ], mtx[ i + 1 ][ j + 1+ 2 * k ],
                        static_cast<T>(r & 1),     msk[ i ][ j + 2 * k] ); 
                r >>= 1;
            }
        }

    #pragma omp for simd schedule(static, 10)
    for( uint32_t i = 1; i < size - 1; i += 2 )
        for( uint32_t j = 1; j < size - 1; j += 2 * kmax ) {
            uint64_t r = generator();
            for(uint32_t k = 0; k < kmax; k++ ) {
                rotate( mtx[ i ][ j + 2 * k ],     mtx[ i ][ j + 1 + 2 * k ],
                        mtx[ i + 1 ][ j + 2 * k ], mtx[ i + 1 ][ j + 1+ 2 * k ],
                        static_cast<T>(r & 1),     msk[ i ][ j + 2 * k] );
                r >>= 1;
            }
        }
}
