clear all
clearvars
Q = load("../output/q.txt");
t = 1:length(Q);

q1 = Q(:, 1);
q2 = Q(:, 2);
q3 = Q(:, 3);
q4 = Q(:, 4);
q5 = Q(:, 5);
q6 = Q(:, 6);

step = 0.01;

plot(t*step, q1, 'DisplayName', '136','LineWidth', 2); hold on;
plot(t*step, q2, 'DisplayName', '135','LineWidth', 2); hold on;
plot(t*step, q3, "DisplayName", "134",'LineWidth', 2); hold on;
plot(t*step, q4, "DisplayName", "133",'LineWidth', 2); hold on;
plot(t*step, q5, "DisplayName", "132",'LineWidth', 2); hold on;
plot(t*step, q6, "DisplayName", "131",'LineWidth', 2); hold on;

xlabel('t, с');
ylabel('q, мкмоль  ');
% grid on;
axis auto

legend