addpath('../result')
clear all
clearvars
set(gcf,'units','pixels','position',[512 512 512 512]);
ax = gca; outerpos = ax.OuterPosition;
ax.Position = [outerpos(1) outerpos(2) outerpos(3) outerpos(4)];
grid(gca, 'off'); 


colormap jet;

S = load("../init/sources.txt");
    
nFrames = 10;

for i = 1:nFrames
    filename = "matrix_" + (i - 1);
    Z = load(filename + ".txt");

    image(imgaussfilt(Z,2))
    numtext = char(strcat(sprintf("%d", i - 1)));
    
%     a=0;
%     for i4 = 1:length(S)
%         for j4 = 1:length(S)
%             if (  S(i4,j4) ~= 0 )
%                 numtext1 = char(strcat(sprintf("%d", a)));
%                 hText1 = text(j4, i4-4, numtext1, 'Color',[0 1 0.1],'FontSize',7);
%                 a = a+1;
%             end
%         end
%     end
    
    hText = text(470, 490, numtext, 'Color',[1 1 0],'FontSize',8);
    saveas(gcf, char(strcat(filename)), 'png');

end
    

% v = VideoWriter('cell.avi','Motion JPEG 2000');
% v.FrameRate = 30;
% open(v)
% 
% for i=2:2048
%     filename = "matrix_" + (i - 1) + ".png";
%     F = imread(char(strcat(filename)));
%     writeVideo(v, F);
%     
% end
% close(v)


