addpath('../imsrc')

clear all
clearvars
set(gcf,'units','pixels','position',[512 512 1300 1300]);
ax = gca; outerpos = ax.OuterPosition;
ax.Position = [outerpos(1) outerpos(2) outerpos(3) outerpos(4)];
grid(gca, 'off'); 
colormap(gray);

I1 = imread('../imsrc/constraints.png');
I2 = imread('../imsrc/initial_conditions.png');
I3 = imread('../imsrc/sources.png');
% imshow(I1)
% imshow(I2)
% imshow(I3)

for i = 1:length(I1)
    for j = 1:length(I1)
        if ( I1(i,j) ~= 0 )
            I1(i,j) = 1;
        end
    end
end

dlmwrite('../init/mask.txt', I1, 'delimiter',' ');

S = zeros(length(I3),length(I3));

for i1 = 1:length(I3)
     for j1 = 1:length(I3)
         if (  (I3(i1,j1) ~= 0) && (I1(i1,j1) ~= 0))
             S(i1,j1) = 1;
         end
     end
 end
 
 dlmwrite('../init/sources.txt', S, 'delimiter',' ');


A = zeros(length(I2),length(I2));

for i2 = 1:length(I2)
    for j2 = 1:length(I2)
        A(i2,j2) = I2(i2,j2);
    end
end

 for i3 = 1:length(A)
     for j3 = 1:length(A)
         A(i3,j3) = round( 100 * A(i3, j3)/255 );
     end
 end


%for i3 = 1:length(I2)
     %for j3 = 1:length(I2)
          %if (  (I3(i3,j3) ~= 0) && (I1(i3,j3) ~= 0))
         %if (  I1(i3,j3) ~= 0)
             %A(i3,j3) = 4;
         %end
     %end
 %end


% 
imagesc(A)

dlmwrite('../init/ca_matrix.txt', A, 'delimiter',' ');


% B = zeros(length(S),length(S));
% B(1,1) = 1;
% imagesc(B)
% 
% a=0;
% for i4 = 1:length(S)
%      for j4 = 1:length(S)
%          if (  S(i4,j4) ~= 0 )
%              numtext = char(strcat(sprintf("%d", a)));
%              hText = text(j4 - 5, i4 - 5, numtext, 'Color',[1 1 0],'FontSize',8);
%              a = a+1;
%          end
%      end
%  end
